var express = require('express');
var router = express.Router();

let mongo = require('mongodb');
var MongoClient = mongo.MongoClient;

MongoClient.connect('mongodb://localhost:27017/tinder', function (err, client) {
  if (err) throw err;
  db = client.db('tinder');
});

router.get('/', function(req, res, next) {
  db.collection('tinder').find({give:true}).toArray().then(function(data){
    res.render('tinder/index', {helpers: data});
  });
});

// ====================
// CREATE
// ====================

router.get('/create', function(req, res, next) {
  res.render('forms/create');
});

router.post('/create', function(req, res, next) {
  db.collection('tinder').insertOne({user:req.body.user, give: (req.body.give !== undefined)});
  res.redirect('/tinder');
});

// ====================
// UPDATE
// ====================

router.get('/update/:id', function(req, res, next) {
  db.collection('tinder').findOne({_id:mongo.ObjectId(req.params.id)}).then(function(data) {
    res.render('forms/update', {data: data});
  });
});

router.post('/update/:id', function(req, res, next) {
  db.collection('tinder').updateOne({_id:mongo.ObjectId(req.params.id)}, {$set: {
    user: req.body.user,
    give: (req.body.give !== undefined)
  }}).then(function(result) {
    if(result.result.ok) res.redirect('/tinder');
    else res.send('failed');
  }).catch(function(error) {
    console.log("rejected: "+error);
  });
});

// ====================
// DELETE
// ====================

router.get('/delete/:id', function(req, res, next) {
  db.collection('tinder')
  .deleteOne({_id: mongo.ObjectId(req.params.id)})
  .then(function(result) {
    // res.send(result.result);
    if(result.result.ok) res.redirect('/tinder');
    else res.send('failed');
  });
});

module.exports = router;
